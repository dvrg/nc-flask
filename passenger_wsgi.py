import sys, os
INTERP = os.path.join(os.environ['HOME'], 'app.crowindojaya.com', 'bin', 'python')
if sys.executable != INTERP:
    os.execl(INTERP, INTERP, *sys.argv)
sys.path.append(os.getcwd())

from app import app as application

if __name__ == "__main__":
    application.run()